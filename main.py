import network
import mqtt
import machine 
import time

WIFI_SSID = "Linksys-E900"
WIFI_PASS = ""
MQTT_HOST = ""
MQTT_PORT = 1883
MQTT_USER = "peba"
MQTT_PASS = ""

# pin = machine.Pin(16, machine.Pin.OUT) for the NodeMCU
pin = machine.Pin(0, machine.Pin.OUT, machine.Pin.PULL_UP)
pin.off()

def connect_to_wifi():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('Connecting to network...')
        sta_if.active(True)
        sta_if.connect(WIFI_SSID, WIFI_PASS)
        while not sta_if.isconnected():
            print('Still not connected...')
            machine.idle()
    print('Connected. Network config: ', sta_if.ifconfig())

def sub_cb(topic, msg): 
    
    message = msg.decode()
    
    if message == "ON":
       print('Message is ON, turning light ON.')
       pin.on()
    
    if message == "OFF":
        print('Message is OFF, turning light OFF.')
        pin.off()

connect_to_wifi()

client = mqtt.MQTTClient("esp-client", MQTT_HOST, user=MQTT_USER, password=MQTT_PASS, port=MQTT_PORT) 
client.set_callback(sub_cb)
client.connect()

client.publish(topic="saltlamp", msg="OFF", qos=1)
print("Sending OFF signal to Broker...")
time.sleep(5) 

while True:
    client.subscribe(topic="saltlamp")
    time.sleep(10)
    print('In While...')
    
#print("Sending ON") 
#client.publish(topic="maintopic/subtopic", msg="ON", qos=1)
#time.sleep(2) 
#print("Sending OFF") 
#client.publish(topic="maintopic/subtopic", msg="OFF", qos=1)
#time.sleep(2)